package gestionbug.demo.rest;

import gestionbug.demo.domain.Bug;
import gestionbug.demo.service.BugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("Bug")
public class BugContrelleur {
    @Autowired
    BugService bugService;
    @GetMapping("")
    public List<Bug> getAll() {
        return bugService.getAll();}

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody Bug bug)
    {
        return bugService.add(bug);
    }
    @PutMapping("")
    public  Bug edite(@RequestBody Bug bug){ return bugService.edit(bug);}

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) { bugService.delete(id);}
    @GetMapping("/searchType")
    public List<Bug> searchType(@PathVariable String type){return bugService.searchType(type);}
    @GetMapping("/searchStatut")
    public List<Bug> searchStatut(@PathVariable String statut){return bugService.searchStatut(statut);}
    @GetMapping("/searchDate")
    public List<Bug> searchDate(@PathVariable Date date){return bugService.searchDate(date);}
}
